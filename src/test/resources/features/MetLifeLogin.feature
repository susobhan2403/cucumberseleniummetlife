Feature: PNBMetlife Website Login and Logout
@Sanity
  Scenario: Sign in and sign out
    When I open PNBMetlife website
    And I sign in
    Then I sign out
