package step_definitions;

import java.io.File;
import java.net.MalformedURLException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

//import io.github.bonigarcia.wdm.WebDriverManager;

import io.cucumber.java.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class Hooks {
	public static WebDriver driver;

	@Before
	public void openBrowser() {
		try{
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--headless");
				options.addArguments("--start-maximized");
				//System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + new File("/chromedriver"));
				System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
				driver = new ChromeDriver(options);
				System.out.println("Automation Driver is " + driver);

		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@After
	/**
	 * Embed a screenshot in test report if test is marked as failed
	 */
	public void embedScreenshot(Scenario scenario) {

		//if (scenario.isFailed()) {
			try {
				scenario.log("Current Page URL is " + driver.getCurrentUrl());
//            byte[] screenshot = getScreenshotAs(OutputType.BYTES);
				byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
				scenario.attach(screenshot, "image/png","ErrorScreenshot");
			} catch (WebDriverException somePlatformsDontSupportScreenshots) {
				System.err.println(somePlatformsDontSupportScreenshots.getMessage());
			}

	//	}
		driver.quit();

	}

}
