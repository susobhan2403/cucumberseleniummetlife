package step_definitions;

import helpers.DataHelper;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import modules.SignInAction;
import modules.SignoutAction;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import pageobjects.LoginPage;
import pageobjects.SignoutPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class MetLifeSignIn {
	public WebDriver driver;
//    public List<HashMap<String,String>> datamap = DataHelper.data();
	public static List<HashMap<String, String>> datamap = null;

	public MetLifeSignIn() {
		driver = Hooks.driver;

		datamap = new ArrayList<HashMap<String, String>>();
		List<HashMap<String, String>> sampleData;
		/*
		 * sampleData.put("username", "susobhan.85"); sampleData.put("password",
		 * "Password1~");
		 */
		sampleData = DataHelper.data();

		System.out.println("Current data" + sampleData);
		datamap.add(sampleData.get(0));

	}

	@When("^I open PNBMetlife website$")
	public void i_open_automationpractice_website() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		driver.get("https://www.pnbmetlife.com/wps/portal/metcustomer");
	}

	@And("^I sign in$")
	public void i_sign_in() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		PageFactory.initElements(driver, LoginPage.class);
		PageFactory.initElements(driver, LoginPage.class);
		SignInAction.Execute(driver, datamap.get(0));
	}

	@Then("^I sign out$")
	public void i_sign_out() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		PageFactory.initElements(driver, SignoutPage.class);
		SignoutAction.Execute(driver);
	}

}
