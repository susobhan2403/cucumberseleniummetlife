package modules;

import helpers.Log;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import pageobjects.SignoutPage;



public class SignoutAction {

	public static void Execute(WebDriver driver) throws Exception{
		
		SignoutPage.logoutLink.click();
		Log.info("Sign out is performed");

		//Reporter.log("Sign out is performed");

	}
}
