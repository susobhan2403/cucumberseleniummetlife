package pageobjects;
import helpers.Log;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
public class SignoutPage extends BaseClass{

	public SignoutPage(WebDriver driver){
		super(driver);
	}    
	
	@FindBy(how=How.XPATH, using="//a[@id='logoutlink']")
	public static WebElement logoutLink;

	}
		

	
	